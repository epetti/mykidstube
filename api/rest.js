var express = require('express');
var app 	= express();
var mysql 	= require("mysql");
var jwt 	= require('jsonwebtoken');
var secret 	= 'derpinaandurmom';
var bcrypt 	= require('bcryptjs');

function REST_ROUTER(router, connection, md5) {
	var self = this;
	self.handleRoutes(router, connection, md5);
}

REST_ROUTER.prototype.handleRoutes = function (router, connection, md5) {

	router.use(function (req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET, POST', 'OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Authorization, x-access-token');

		// Fixes pre-flight 500 COR error
		if ('OPTIONS' == req.method) {
			res.send(200);
		} else {
			next();
		}
		
	});

	router.post('/', function(req, res) {
		console.log('HI API');
	});

	// Authenticate the user
	router.post('/auth', function (req, res) {
		var query = "SELECT * from USERS where UserEmail = ?";
		var table = [req.body.data.UserEmail];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			// Return error message if there is a db error
			if (err) {
				res.json({
					"Error": "Error looking up user."
				});
			} else {
				console.log(rows);
				if (rows <= 0) {
					res.json({
						"Error": "No such user"
					});
					return;
				}
				// Check if return object is valid or not
				validHash = bcrypt.compareSync(req.body.data.UserPass, rows[0].UserPass);

				if (validHash) {

					var token = jwt.sign(rows[0].UserID, secret, {});
					res.json({
						"valid": true,
						"loggedIn": true,
						"userid": rows[0].UserID,
						"token": token
					});

				} else {
					// Return invlaid login
					res.json({
						"valid": false,
						"message": "Inccorect password"
					});
				}
			}
		});
	});

	// Check if user exists before adding a new one
	router.get('/checkUser/:email', function (req, res, next) {

		var query = 'SELECT UserID FROM users WHERE UserEmail = ?';
		var table = [req.params.email];
		query = mysql.format(query, table);
		connection.query(query, function(err, rows) {
			if (err) {
				res.json({
					"valid": true
				});
			} else {
				if (rows.length > 0) {
					res.json({
						"valid": false,
						"message": "User already exists."
					});
				} else {
					res.json({
						"valid": true
					});
				}
			}
		});

	});

	router.post('/newUser', function (req, res) {

		// Validate the form (should also be done on the front-end)
		if (!req.body.data.email || !req.body.data.password) {
			res.json({
				"valid": false
			});
			return;
		}

		// Salt/Hash PW and insert new user into db
		var query = 'INSERT INTO users (UserEmail, UserPass) VALUES(?, ?)';
		var email = req.body.data.email;
		var salt = bcrypt.genSaltSync(10);
		var hash = bcrypt.hashSync(req.body.data.password, salt);
		var table = [email, hash];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({
					"success": false
				});
			} else {
				res.json({
					"success": true
				});
			}
		});
	});

	// Secure router
	function ensureAuthorized(req, res, next) {
	    var bearerToken;
	    var bearerHeader = req.headers["authorization"];
	    if (typeof bearerHeader !== 'undefined') {
	        var bearer = bearerHeader.split(" ");
	        bearerToken = bearer[1];
	        var token = bearerHeader;

	        jwt.verify(token, secret, function (err, decoded) {
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });
				} else {
					req.decoded = decoded;
					next();
				}
			});

	    } else {
	        res.sendStatus(403);
	    }
	}

	// Get List by User
	// Gets all the lists associated with this user
	router.get('/lists/:userid', ensureAuthorized, function (req, res, next) {
		// Query gets the 1st image related to the selected list and joins it to the lists table
		var query = 'SELECT vl.*, MIN(v.VideoImage) AS VideoImage FROM videolists vl JOIN videos v ON v.ListID = vl.ListID WHERE vl.UserID = ? GROUP BY vl.ListID';
		var table = [req.params.userid];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({"Error" : true, "Message" : "Error executing MySQL query"});
			} else {
				res.json({"Lists" : rows});
			}
		});
	});
	
	// Create New List
	router.post('/newList', ensureAuthorized, function (req, res, next) {
		var query = 'INSERT INTO ?? (??, ??, ??, ??) VALUES (?, ?, ?, ?)';
		var table = ['videolists', 'UserID', 'ListName', 'ListAuthor', 'CreatedOn', req.body.data.userid, req.body.data.name, req.body.data.author, req.body.data.date];
		query =  mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({'Error' : true });
			} else {
				res.json({'Error' : false, 'Message' : "List Added!"});
			}
		});
	});
	
	// Add Video
	router.post('/addVideo', ensureAuthorized, function (req, res, next) {
		var query = "INSERT INTO ?? (??, ??, ??, ??, ??) VALUES (?, ?, ?, ?, ?)";
		var table = ['videos', 'ListID', 'VideoTitle', 'VideoAuthor', 'VideoPath', 'VideoImage', req.body.data.listId, req.body.data.title, req.body.data.author, req.body.data.path, req.body.data.image];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({'Error' : true, 'Message' : err });
			} else {
				res.json({'Error' : false, 'Message' : 'Video Added'});
			}
		});
	});
	
	// Get Videos By List Id
	router.get('/getVideos/:listId', ensureAuthorized, function (req, res, next) {
		var query = 'SELECT * FROM ?? WHERE ??=?';
		var table = ['videos', 'ListID', req.params.listId];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({"Error" : true, "Message" : err });
			} else {
				res.json({'Videos' : rows});
			}
		}) 
	});

	router.get('/user/:userName', ensureAuthorized, function (req, res, next) {
		var query = 'SELECT * FROM ?? WHERE ??=?';
		var table = ['users', 'UserName', req.params.userName];
		query = mysql.format(query, table);
		connection.query(query, function (err, rows) {
			if (err) {
				res.json({"Error" : true, "Message" : "Error executing MySQL query"});
			} else {
				res.json({'User' : rows});
			}
		});
	});
}

module.exports = REST_ROUTER;