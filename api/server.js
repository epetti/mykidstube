var express     = require('express');
var mysql       = require('mysql');
var bodyParser  = require('body-parser');
var jwt         = require('jsonwebtoken');
var md5         = require('MD5');
var config      = require('./config.js');
var rest        = require('./rest.js');
var app         = express();

function REST(){
    var self = this;
    self.connectMysql();
};

// Set up db connnection
REST.prototype.connectMysql = function() {
    var self = this;
    var pool = mysql.createPool({
        connectionLimit : 100,
        host     : 'localhost',
        user     : 'myKidsTube',
        password : 'UvwYeDJYzdqdTFzn',
        database : 'myKidsTube',
        debug    :  false
    });
    pool.getConnection(function(err,connection){
        if(err) {
          self.stop(err);
        } else {
          self.configureExpress(connection);
        }
    });
}

// Set up express with initial URL as /api
REST.prototype.configureExpress = function(connection) {
      var self = this;
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json());
      var router = express.Router();
      app.use('/api', router);
      var rest_router = new rest(router,connection,md5);
      self.startServer();
      app.set('superSecret', config.secret);
}

// Start method
REST.prototype.startServer = function() {
      app.listen(3000,function(){
          console.log("All right ! I am alive at Port 3000.");
      });
}

// Stop method
REST.prototype.stop = function(err) {
    console.log("ISSUE WITH MYSQL \n" + err);
    process.exit(1);
}

// Instantiate the server object
new REST();