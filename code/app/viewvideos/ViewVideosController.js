myKidsTube.controller('ViewVideosCtrl', function(
	// Dependencies
	$scope,
	$log,
	$http,
	$localStorage,
	$location
	){

	init();

	function init() {
		$scope.videos = [];
		// Grabbing the list id out of session
		$scope.$storage = $localStorage;
		$scope.viewListId = $scope.$storage.currentListId;
		$scope.currentVideoIndex = 0;

		// YT Params
		$scope.playerVars = {
			rel: 0
		};

		// If no id is set, redirect them back to the home page
		if ($scope.viewListId != null) {

			getVideos($scope.viewListId);

		} else {

			$location.path('/home');

		}

		// Auto play event to play the next movie in the list
		$scope.$on('youtube.player.ended', function($event, player) {

			// Check if we're on the last video, if we are reset the count
			if ($scope.currentVideoIndex === $scope.videos.length) {

				$scope.currentVideoIndex = 0;

			} else {

				$scope.currentVideoIndex++;

			}

			// Play next movie
			playMovie($scope.videos[$scope.currentVideoIndex].VideoPath);
			
		});
	}

	$scope.getRandomClass = function(){
		return "tilt-" + Math.floor((Math.random()*3)+1);
	}

	$scope.getRandomClass = function(){
		return "tilt-" + Math.floor((Math.random()*3)+1);
	}

	function getVideos(id) {
		// Grabs all the videos contained in the list
		$http.get('http://localhost:3000/api/getVideos/' + id + '/', {
			headers: {
				'authorization': $scope.$storage.token
			}
		})
		.success(function(data) {
			$log.info(data);
			$scope.videos = data.Videos; // parse the video data
		})
		.error(function() {
			$log.info("Error getting the video data");
		});

	}

	function playMovie(path) {
		$scope.videoPlayer = path;
	}

	// Plays the video when clicked
	$scope.rollMovie = function(path, index) {
		playMovie(path);
		$scope.currentVideoIndex = index;
	}

});