myKidsTube.controller('NavCtrl', function(
	
	$scope,
	$localStorage,
	$location
	){

	init();

	function init() {

		// Setting up local var for storage
		$scope.$storage = $localStorage;

	}

	$scope.logOut = function() {

		// Clear out locally stored user data
		$scope.$storage.$reset();

		// Redirect to login page
		$location.path('/login');
		
	}

});