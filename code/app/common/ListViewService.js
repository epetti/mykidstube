myKidsTube.service('ListViewService', function($log) {

	var listId = null;

	return {

		setListId: function(id) {
			listId = id;
		},

		getListId: function() {
			return listId;
		}

	}	

});