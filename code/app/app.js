var myKidsTube = angular.module('myKidsTube', ['ui.router', 'ngCordova', 'ngStorage', 'youtube-embed']);

myKidsTube.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

	$urlRouterProvider.otherwise('/home');

	$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: 'app/home/home.html'
		})
		.state('search', {
			url: '/search',
			templateUrl: 'app/search/search.html'
		})
		.state('viewvideos', {
			url: '/viewvideos',
			templateUrl: 'app/viewvideos/viewvideos.html'
		})
		.state('login', {
			url: '/login',
			templateUrl: 'app/login/login.html'
		});
});