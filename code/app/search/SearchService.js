myKidsTube.service('SearchService', function(
	// Dependencies
	$http,
	$log,
	$sce
	){
	
	this.searchVideos = function(term) {
		
		var videos = [];

		$http.get('https://www.googleapis.com/youtube/v3/search', {
			params: {
				key: 'AIzaSyCrS5wjPo9sso9TjoMMFj-UwLzfH4oFwe8',
				type: 'video',
				maxResults: '8',
				part: 'id,snippet',
				fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/medium,items/snippet/channelTitle',
				q: term
			}
		})
		.success( function(data) {
			for (var i = 0; i < data.items.length; i++) {
				videos.push({
					id: data.items[i].id.videoId,
					title: data.items[i].snippet.title,
					path: $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + data.items[i].id.videoId + "?autoplay=1&origin=http://sytewerks.com"),
					imagePath: data.items[i].snippet.thumbnails.medium.url,
					channel: data.items[i].snippet.channelTitle
				});
			}
			$log.info('raw-data');
			$log.info(data);
		})
		.error( function () {
			$log.info('Search error');
		});

		return videos;

	},

	this.addVideos = function(videoData, token) {
		// Get what's already in storage, parse/push into array
		// add new video, resave json

		// return JSON.stringify(video);
		$http.post('http://localhost:3000/api/addVideo', {
			headers: {
				'authorization': token
			},
			data: {
				listId: 1,
				title: videoData.title,
				author: videoData.channel,
				path: videoData.id,
				image: videoData.imagePath
			}
		})
		.success( function(data) {
			$log.info(data);
		})
		.error( function() {
			$log.info("didn't work");
		});
	}

});