myKidsTube.controller('LoginCtrl', function(
	// Dependencies
	$scope,
	$http,
	$log,
	$localStorage,
	$location
	){

	function init() {
		$scope.isNewUser = false;
	}

	$scope.signIn = function(fields) {

		console.log(fields);
		$http.post('http://localhost:3000/api/auth', {
			"data": {
				"UserEmail": fields.UserEmail,
				"UserPass": fields.UserPass
			}
		})
		.success( function(data) {
			$scope.$storage = $localStorage.$default({
				token: data.token,
				loggedIn: data.loggedIn,
				userid: data.userid
			});
			console.log($scope.$storage);

			// Redirect to home page after login
			$location.path('/home');
		})
		.error( function(data) {
			$log.info(data);
		});

	}

	$scope.signOut = function() {

	}

	$scope.startNewUser = function() {

		// Toggle whether or not you're creating a new user account
		if ($scope.isNewUser) {

			$scope.isNewUser = false;

		} else {

			$scope.isNewUser = true;

		}

	}

	$scope.newUser = function(userData) {

		alert(userData.UserEmail);
		$scope.passwordMatch = true;

	}	

});