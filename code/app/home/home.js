myKidsTube.controller('HomeCtrl',  function(
	// Dependencies
	$log,
	$http,
	$scope,
	$localStorage,
	$sessionStorage,
	$location
	){

/*	document.addEventListener('deviceready', function() {
		checkForVideos();
	});*/

	init();

	function init() {

		$scope.$storage = $localStorage;
		console.log($scope.$storage.token);

		if (!$scope.$storage.userid) {

			$location.path('/login');
			console.log('not logged in');

		} else {

			getUserLists($scope.$storage.userid);
			
		}
	}
	
	// Show Lists assoicated with this user
	function getUserLists(userId) {

		$scope.lists = [];	// Storing the list information
		
		// Get all the lists assoicated with this user
		$http.get('http://localhost:3000/api/lists/' + userId + '/', {
				headers: {
					'authorization': $scope.$storage.token
				}
			})
			.success(function(data) {
				$log.info("Got Lists");
				$scope.lists = data.Lists; // Parse the list data
			})
			.error(function() {
				$log.info("Error getting lists");
			});
	}

	// When a list is clicked on, take them to the video list
	$scope.ChooseList = function(listId) {
		$scope.$storage.currentListId = listId; // Storing the list id in session
		$location.path('/viewvideos'); // Switching view to video list view
	}

});